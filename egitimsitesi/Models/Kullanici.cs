//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace egitimsitesi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public partial class Kullanici
    {
        public int KullaniciID { get; set; }
        [DisplayName("Isim ve Soyad")]
        
        public string isimvesoy { get; set; }
        [DisplayName("Il")]
        public string il { get; set; }
        [DisplayName("Telefon")]
        public string telefonNumarasi { get; set; }
        [DisplayName("Dogum Tarihi")]
        public System.DateTime dgmTarihi { get; set; }
        [DisplayName("Eposta")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string eposta { get; set; }
        public string Role { get; set; }

        public int KodlamaId { get; set; }
        [DisplayName("Hatirla")]
        public bool hatirla { get; set; }
        [DisplayName("Parola")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "En az 6 karakter.")]
        public string parola { get; set; }
    
        public virtual Kodlama Kodlama { get; set; }
    }
}
