﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace egitimsitesi.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class dil {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal dil() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("egitimsitesi.Resources.dil", typeof(dil).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ana Sayfa.
        /// </summary>
        public static string AnaSayfa {
            get {
                return ResourceManager.GetString("AnaSayfa", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Çıkış.
        /// </summary>
        public static string Çıkış {
            get {
                return ResourceManager.GetString("Çıkış", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dil.
        /// </summary>
        public static string Dil {
            get {
                return ResourceManager.GetString("Dil", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Düzenle.
        /// </summary>
        public static string Düzenle {
            get {
                return ResourceManager.GetString("Düzenle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Giriş.
        /// </summary>
        public static string Giriş {
            get {
                return ResourceManager.GetString("Giriş", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Güncelle.
        /// </summary>
        public static string Güncelle {
            get {
                return ResourceManager.GetString("Güncelle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hakkında.
        /// </summary>
        public static string Hakkında {
            get {
                return ResourceManager.GetString("Hakkında", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İletişim.
        /// </summary>
        public static string İletişim {
            get {
                return ResourceManager.GetString("İletişim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kaydol.
        /// </summary>
        public static string Kaydol {
            get {
                return ResourceManager.GetString("Kaydol", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kullanıcı.
        /// </summary>
        public static string Kullanıcı {
            get {
                return ResourceManager.GetString("Kullanıcı", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Oluştur.
        /// </summary>
        public static string Oluştur {
            get {
                return ResourceManager.GetString("Oluştur", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sil.
        /// </summary>
        public static string Sil {
            get {
                return ResourceManager.GetString("Sil", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sohbet.
        /// </summary>
        public static string Sohbet {
            get {
                return ResourceManager.GetString("Sohbet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni.
        /// </summary>
        public static string Yeni {
            get {
                return ResourceManager.GetString("Yeni", resourceCulture);
            }
        }
    }
}
