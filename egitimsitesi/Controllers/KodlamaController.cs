﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using egitimsitesi.Models;

namespace egitimsitesi.Controllers
{
    public class KodlamaController : Controller
    {
        private egtmSitesiEntities1 db = new egtmSitesiEntities1();

        // GET: Kodlama
        public ActionResult Index()
        {
            return View(db.Kodlama.ToList());
        }

        // GET: Kodlama/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kodlama kodlama = db.Kodlama.Find(id);
            if (kodlama == null)
            {
                return HttpNotFound();
            }
            return View(kodlama);
        }

        // GET: Kodlama/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Kodlama/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "KodlamaID,StringArea,minStringArea")] Kodlama kodlama)
        {
            if (ModelState.IsValid)
            {
                db.Kodlama.Add(kodlama);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kodlama);
        }

        // GET: Kodlama/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kodlama kodlama = db.Kodlama.Find(id);
            if (kodlama == null)
            {
                return HttpNotFound();
            }
            return View(kodlama);
        }

        // POST: Kodlama/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "KodlamaID,StringArea,minStringArea")] Kodlama kodlama)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kodlama).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kodlama);
        }

        // GET: Kodlama/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kodlama kodlama = db.Kodlama.Find(id);
            if (kodlama == null)
            {
                return HttpNotFound();
            }
            return View(kodlama);
        }

        // POST: Kodlama/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            Kodlama kodlama = db.Kodlama.Find(id);
            db.Kodlama.Remove(kodlama);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
