﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using egitimsitesi.Models;

namespace egitimsitesi.Controllers
{
    public class KullaniciController : Controller
    {
        private egtmSitesiEntities1 db = new egtmSitesiEntities1();

        // GET: Kullanici
        [Authorize(Roles ="Admin")]
        public ActionResult Index()
        {
            var kullanici = db.Kullanici.Include(k => k.Kodlama);
            return View(kullanici.ToList());
        }

        public ActionResult C()
        {
            ViewBag.c = db.Kodlama.Where(s => s.KodlamaID == 1).Select(s => s.StringArea).Single();
            return View();
        }
        public ActionResult Cplusplus()
        {
            ViewBag.cpp = db.Kodlama.Where(s => s.KodlamaID == 2).Select(s => s.StringArea).Single();
            return View();
        }
        public ActionResult Csharp()
        {
            ViewBag.csharp = db.Kodlama.Where(s => s.KodlamaID == 3).Select(s => s.StringArea).Single();
            return View();
        }
        public ActionResult Java()
        {
            ViewBag.java = db.Kodlama.Where(s => s.KodlamaID == 4).Select(s => s.StringArea).Single();
            return View();
        }
        // GET: Kullanici/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanici kullanici = db.Kullanici.Find(id);
            if (kullanici == null)
            {
                return HttpNotFound();
            }
            return View(kullanici);
        }

        // GET: Kullanici/Create
        public ActionResult Create()
        {
            ViewBag.KodlamaId = new SelectList(db.Kodlama, "KodlamaID", "StringArea");
            return View();
        }

        // POST: Kullanici/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KullaniciID,isimvesoy,il,telefonNumarasi,dgmTarihi,eposta,Role,KodlamaId,hatirla,parola")] Kullanici kullanici)
        {
            if (ModelState.IsValid)
            {
                db.Kullanici.Add(kullanici);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }

            ViewBag.KodlamaId = new SelectList(db.Kodlama, "KodlamaID", "StringArea", kullanici.KodlamaId);
            return View(kullanici);
        }

        // GET: Kullanici/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanici kullanici = db.Kullanici.Find(id);
            if (kullanici == null)
            {
                return HttpNotFound();
            }
            ViewBag.KodlamaId = new SelectList(db.Kodlama, "KodlamaID", "StringArea", kullanici.KodlamaId);
            return View(kullanici);
        }

        // POST: Kullanici/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit([Bind(Include = "KullaniciID,isimvesoy,il,telefonNumarasi,dgmTarihi,eposta,Role,KodlamaId,hatirla,parola")] Kullanici kullanici)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kullanici).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KodlamaId = new SelectList(db.Kodlama, "KodlamaID", "StringArea", kullanici.KodlamaId);
            return View(kullanici);
        }

        // GET: Kullanici/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanici kullanici = db.Kullanici.Find(id);
            if (kullanici == null)
            {
                return HttpNotFound();
            }
            return View(kullanici);
        }

        // POST: Kullanici/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Kullanici kullanici = db.Kullanici.Find(id);
            db.Kullanici.Remove(kullanici);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
