﻿using egitimsitesi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace egitimsitesi.Controllers
{
    public class HomeController : Controller
    {
        private egtmSitesiEntities1 db = new egtmSitesiEntities1();
        public ActionResult Index(string arama)
        {
            if (!String.IsNullOrEmpty(arama))
            {
                ViewBag.c = db.Kodlama.Where(s => s.KodlamaID == 1).Select(s => s.minStringArea).Single().Contains(arama);
                ViewBag.cpp = db.Kodlama.Where(s => s.KodlamaID == 2).Select(s => s.minStringArea).Single().Contains(arama);
                ViewBag.csharp = db.Kodlama.Where(s => s.KodlamaID == 3).Select(s => s.minStringArea).Single().Contains(arama);
                ViewBag.java = db.Kodlama.Where(s => s.KodlamaID == 4).Select(s => s.minStringArea).Single().Contains(arama);
            } else {
                ViewBag.c = db.Kodlama.Where(s => s.KodlamaID == 1).Select(s => s.minStringArea).Single();
                ViewBag.cpp = db.Kodlama.Where(s => s.KodlamaID == 2).Select(s => s.minStringArea).Single();
                ViewBag.csharp = db.Kodlama.Where(s => s.KodlamaID == 3).Select(s => s.minStringArea).Single();
                ViewBag.java = db.Kodlama.Where(s => s.KodlamaID == 1).Select(s => s.minStringArea).Single();
            }
            return View();
        }
        public ActionResult DilBilgisi(string dil, string sayfa)
        {
            Session["dil"] = new System.Globalization.CultureInfo(dil);
            return Redirect(sayfa);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Hakkında kısmına hoş geldiniz!";

            return View();
        }
        public ActionResult Chat()
        {
            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "İletişime Hoş Geldiniz!";

            return View();
        }
    }
}